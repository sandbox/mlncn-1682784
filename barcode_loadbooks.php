<?php

require './includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);
module_load_include('inc', 'node', 'node.pages');

ini_set('memory_limit', '128M');

user_authenticate('Cambridge Library', 'biolib');

$xmlfile=$argv[1];

// Parsing a large document with Expand and SimpleXML
#$xmlfile="test.xml";


// Create a new node

readXMLBooks($xmlfile);

function readXMLBooks ($xmlfile) {
	$reader = new XMLReader();
	$reader->open($xmlfile);
	
	while ($reader->read()) {
#		echo memory_get_usage() . " read XML book record\n";
		switch ($reader->nodeType) {
			case (XMLREADER::ELEMENT):
			if ($reader->localName == "Book") {
				$xmlnode = $reader->expand();
				$dom = new DomDocument();
				$n = $dom->importNode($xmlnode,true);
				$dom->appendChild($n);
				$x = simplexml_import_dom($n);
				
				# Set up Book Array
				unset($book); // Clearing the array
				$title=preg_replace("/[\n\s]+/"," ",$x->Title);
				$book['Title']=$title;
				
				$description="<h3>".$x->Amazon->Reviews->Review[0]->Source."</h3>".
					$x->Amazon->Reviews->Review[0]->Content."\n<h3>".
					$x->Amazon->Reviews->Review[1]->Source."</h3>".
					$x->Amazon->Reviews->Review[1]->Content."\n";
				$book['Description']=$description;

				$book['ISBN10']=(string)$x->ISBN10;
				$book['ISBN13']=(string)$x->ISBN13;
				$book['ItemID']=(string)$x->CatID;				
				
				$book['Owner']=(string)$x->Owner;
				$book['OwnerUID']=getUIDfromUserName($book['Owner']);
				if (!$book['OwnerUID']) {
					print "Book Owner: ".$book['Owner']." for Book ItemID: ".$book['ItemID']." doesn't exist\n";
				}
				$book['Loanee']='';	
				$book['LoaneeUID']=getUIDfromUserName($book['Loanee']);			

				$book['Dewey']=(string)$x->DDC;
				$book['LCC']=(string)$x->LCC;
				$book['Publisher']=(string)$x->Publisher;

				preg_match("(\d{4,4})",$x->PubDate,$pubdate);
				$book['PubDate']=$pubdate[0];

				foreach ($x->Authors->Author as $author) {
					$book['Authors'][] = (string)$author;
				}
				foreach ($x->Subjects->Subject as $subject) {
					$book['Subjects'][]=(string)$subject;
				}
				$book['ASIN']=(string)$x->Amazon->ASIN;								
				$book['CoverImageLink']=(string)$x->Amazon->Images->MediumImage->img['src'];
				$book['AmazonLink']=(string)$x->Amazon->URL;
				$book['NumPages']=(string)$x->Pages;
				
				loadBook($book);
				unset($xmlnode);
				unset($dom);
				unset($n);
				unset($x);
			}
		}
	}	
}

function loadBook ($book) {
	global $xmlfile;
#	echo memory_get_usage() . " Starting to load book\n"; 
	$node = new stdClass();
	$node->type = 'book';
    node_object_prepare($node);
    $node->uid = 5746;
#	$node->name = 'haypod@gmail.com';
    $node->title = $book['Title'];
    $node->body = (string)$book['Description'];
    $node->teaser = "";
    $node->filter = variable_get('filter_default_format', 2);
    $node->format = 2;
    $node->language = 'en';
    $node->revision = 0;
    $node->promote = 0;
    $node->created = time();
    $node->comment= 2;
    
	$node->field_isbn10[0]['value']=$book['ISBN10'];
	$node->field_isbn13[0]['value']=$book['ISBN13'];
	$node->field_owner[0]['uid']= $book['OwnerUID'];
	$node->field_amazonitem[0]['asin'] =$book['ASIN'];
	
	foreach ($book['Authors'] as $author) {
		$node->field_authors[]['value'] = $author;
	}
	
	$node->field_loanee[0]['uid'] =	'';
	$node->field_itemid[0]['value'] =	$book['ItemID'];
	$node->field_deweydecimal[0]['value'] = $book['Dewey'];
	$node->field_lcnum[0]['value'] = $book['LCC'];
	$node->field_publisher[0]['value'] = $book['Publisher'];
	
	preg_match("(\d{4,4})",$x->PubDate,$pubdate);
	$node->field_pubdate[0]['value'] = $book['PubDate'];
	
	
	foreach ($book['Subjects'] as $subject) {
		$node->field_keywords[]['value'] = $subject;
	}
	
	$node->field_bookcoverlink[0]['url']=$book['CoverImageLink'];
	$node->field_bookcoverlink[0]['title']='Book Cover';	        		
	$node->field_amazonlink[0]['title'] =	$book['Title'];
	$node->field_amazonlink[0]['url'] =	$book['AmazonLink'];
	
#	echo memory_get_usage() . " Before node load\n";
	$node = node_submit($node);
	node_save($node);  //Actually save or edit the node now
	content_insert($node);

    $nid = $node->nid;
	print "$xmlfile  NodeID: $nid  Title= ".$book['Title']."\n";
	printf ("Memory used: %4d\n",memory_get_usage()/1000000); 
	unset($node);
#	echo memory_get_usage() . " After unsetting $node\n"; 

}


function getUIDfromUserName($username) {
  $sql = "SELECT * FROM {users} WHERE name = '%s'";
  $result = db_query($sql,$username);
  $username = db_fetch_object($result);
  return $username->uid;
}


?>
