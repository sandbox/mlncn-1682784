// Order Book Modal Dialog
$(document).ready(function() {
    $(function() {
        $("#orderbook-dialog").dialog({
            bgiframe: true,
            autoOpen: false,
            modal: true,
            width: 600,
            height: 400,
            overlay: {
              backgroundColor: '#000',
              opacity: 0.5
            },
            buttons: {
              'Close': function() {$(this).dialog('close');}
            }
        });


        $('#orderbook').click(function() {
            $('#orderbook-dialog').dialog('open');
        })
        .hover(
        function() {
            $(this).addClass("ui-state-hover");
        },
        function() {
            $(this).removeClass("ui-state-hover");
        }
        ).mousedown(function() {
            $(this).addClass("ui-state-active");
        })
        .mouseup(function() {
            $(this).removeClass("ui-state-active");
        });
    });
});


